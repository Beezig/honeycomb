package eu.beezig.honeycomb.obsclient;

import com.google.crypto.tink.subtle.Ed25519Sign;
import com.google.crypto.tink.subtle.X25519;
import org.json.JSONObject;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;
import java.util.UUID;
import java.util.function.Consumer;

public class ObsState {
    private byte[] x25519Private;
    private Ed25519Sign secretKey;
    private UUID app;
    private Consumer<Boolean> authListener;

    public void setAuthListener(Consumer<Boolean> authListener) {
        this.authListener = authListener;
    }

    public void initialize() throws Exception {
        generateAppId();
        sendAuthRequest();
    }

    private void generateAppId() {
        this.app = UUID.randomUUID();
    }

    private void sendAuthRequest() throws Exception {
        String pub = generatePubKey();
        JSONObject json = new JSONObject();
        json.put("uuid", app.toString());
        json.put("name", "Test");
        json.put("public_key", pub);
        String res = HttpUtil.sendPost("/register", json.toString());
        JSONObject parsed = new JSONObject(res);
        setSecretKey(parsed.getString("key"), parsed.getString("shared_public"));
    }

    private String generatePubKey() {
        x25519Private = X25519.generatePrivateKey();
        try {
            return Base64.getEncoder().encodeToString(X25519.publicFromPrivate(x25519Private));
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setSecretKey(String encrypted, String serverPublic) throws Exception {
        Base64.Decoder dec = Base64.getDecoder();
        byte[] encryptedBytes = dec.decode(encrypted);
        byte[] serverPublicKey = dec.decode(serverPublic);
        byte[] sharedSecret = X25519.computeSharedSecret(x25519Private, serverPublicKey);
        Arrays.fill(x25519Private, (byte) 0);
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(sharedSecret, "AES"), new GCMParameterSpec(128, new byte[12]));
        byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
        secretKey = new Ed25519Sign(decryptedBytes);
        if(authListener != null) authListener.accept(true);
    }

    private String signMessage(String msg) throws GeneralSecurityException {
        if(msg.isEmpty()) msg = "obs-controller";
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(msg.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(secretKey.sign(digest.digest()));
    }

    private boolean isAuthenticated() {
        return secretKey != null;
    }

    public void startRecording(String fileName) throws Exception {
        if(!isAuthenticated()) return;
        HttpUtil.sendPost("/recording/start", fileName, signMessage(fileName), app.toString());
    }

    public void stopRecording() throws Exception {
        if(!isAuthenticated()) return;
        HttpUtil.sendPost("/recording/stop", "", signMessage(""), app.toString());
    }
}
