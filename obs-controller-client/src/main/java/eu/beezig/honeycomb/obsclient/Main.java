package eu.beezig.honeycomb.obsclient;

public class Main {
    public static void main(String[] args) {
        ObsState state = new ObsState();
        ControllerScreen.show(state);
    }
}
