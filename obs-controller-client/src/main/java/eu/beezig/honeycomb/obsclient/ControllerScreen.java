package eu.beezig.honeycomb.obsclient;

import javax.swing.*;

public class ControllerScreen {
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JButton authenticateRegisterNewAppButton;
    private JButton stopRecordingButton;
    private JTextField textField1;
    private JButton startRecordingButton;
    private JLabel statusLabel;


    public static void show(ObsState state) {
        JFrame frame = new JFrame("ControllerScreen");
        ControllerScreen controllerScreen = new ControllerScreen();
        controllerScreen.setState(state);
        frame.setContentPane(controllerScreen.panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public void setState(ObsState obsState) {
        authenticateRegisterNewAppButton.addActionListener(actionEvent -> {
            try {
                obsState.initialize();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        stopRecordingButton.addActionListener(actionEvent -> {
            try {
                obsState.stopRecording();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        startRecordingButton.addActionListener(actionEvent -> {
            try {
                obsState.startRecording(textField1.getText().trim());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        obsState.setAuthListener(auth -> statusLabel.setText("Status: " + (auth ? "Authenticated" : "Not authenticated")));
    }
}
