package eu.beezig.honeycomb.obsclient;

import org.apache.commons.io.IOUtils;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class HttpUtil {
    private static final String BASE = "http://localhost:8085";

    public static String sendPost(String urlString, String body, String signature, String appId) throws Exception {
        URL url = new URL(BASE + urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.addRequestProperty("Content-Type", "application/json");
        conn.addRequestProperty("X-OBSC-App", appId);
        conn.addRequestProperty("X-OBSC-Signature", signature);
        conn.connect();
        try(OutputStream os = conn.getOutputStream()) {
            os.write(body.getBytes(StandardCharsets.UTF_8));
        }
        return IOUtils.toString(conn.getInputStream(), StandardCharsets.UTF_8);
    }

    public static String sendPost(String urlString, String body) throws Exception {
        URL url = new URL(BASE + urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.addRequestProperty("Content-Type", "application/json");
        conn.connect();
        try(OutputStream os = conn.getOutputStream()) {
            os.write(body.getBytes(StandardCharsets.UTF_8));
        }
        return IOUtils.toString(conn.getInputStream(), StandardCharsets.UTF_8);
    }

    public static String sendGet(String urlString) throws Exception {
        URL url = new URL(BASE + urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        return IOUtils.toString(conn.getInputStream(), StandardCharsets.UTF_8);
    }
}
